<?php

class Muffin_CF7_Local_Json
{
	function __construct()
	{
		// When a CF7 form is updated (either created or saved),
		// stores its data as JSON and HTML
		add_action( 'wpcf7_after_update', array( $this, 'save_to_json' ), 10, 1 );
	}

	/**
	 * Save form data
	 */
	function save_to_json( $wpcf7 )
	{
		// Save directory, in the [child] theme
		$save_dir = get_stylesheet_directory() . '\\wpcf7-json\\';

		// Create the save directory if it doesn't already exist
		$this->create_save_directory( $save_dir );

		// Build the saved data object
		$form_data = $this->wpcf7_object_to_json( $wpcf7 );

		// Path to file, minus extension. Needs either '.json' or '.html' to complete the path
		$path_base = wp_normalize_path( $save_dir . $form_data['id'] );

		// Write JSON
		$this->write_file( $path_base . '.json', $form_data['json'] );

		// Write HTML
		$this->write_file( $path_base . '.html', $form_data['html'] );
	}

	/**
	 * Build am array object, to be converted to JSON later
	 *
	 * @param  Object  $wpcf7  WPCF7_ContactForm instance
	 */
	function wpcf7_object_to_json( $wpcf7 )
	{
		// User meta
		$user = wp_get_current_user();

		$form_obj = array(
			'id'    => $wpcf7->id(),
			'name'  => $wpcf7->name(),
			'title' => $wpcf7->title(),

			// Properties
			'form'     => $wpcf7->prop( 'form' ),
			'mail'     => $wpcf7->prop( 'mail' ),
			'mail_2'   => $wpcf7->prop( 'mail_2' ),
			'messages' => $wpcf7->prop( 'messages' ),
			'additional_settings' => $wpcf7->properties["additional_settings"],

			// Meta
			'modified' => array (
				'date' => date( 'Y-m-d H:i:s' ),
				'domain' => $_SERVER['HTTP_HOST'],
				'user' => array(
					'id' => $user->user_login,
					'username' => $user->ID,
					'displayname' => $user->display_name ,
				),
			)
		);

		$form_data = array(
			'id'   => $wpcf7->id(),
			'json' => json_encode( $form_obj, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT ),
			'html' => $wpcf7->prop( 'form' ),
		);

		return $form_data;
	}

	function create_save_directory( $save_dir )
	{
		// Make empty dir if it doesn't exist
		if ( !file_exists( $save_dir ) )
		{
			mkdir( $save_dir, 0777, true );

			// Save an htaccess file to this dir, to prevent direct access
			$htaccess_file_path = wp_normalize_path( $save_dir . '.htaccess' );
			$htaccess_file_data = 'Deny from all';
			$this->write_file( $htaccess_file_path, $htaccess_file_data );
		}
	}

	function write_file( $path, $file_data )
	{
		$file = fopen( $path, 'w' );

		if ( $file )
		{
			fwrite( $file, $file_data );
			fclose( $file );
		}
	}
}

$Muffin_CF7_Local_Json_instance = new Muffin_CF7_Local_Json();
