<?php
/*
Plugin Name: Contact Form 7 - Local JSON & HTML (Versioning) [WIP]
Plugin URI:  https://codemuffin.com
Description: Save CF7 forms as JSON and HTML. Used for versioning your forms.
Author:      Code Muffin
Author URI:  https://codemuffin.com
Version:     1.0.0
License:     GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

if ( ! defined( 'WPINC' ) )
{
	die;
}

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) )
{
	require( 'admin/class-cf7-local.php' );
}
