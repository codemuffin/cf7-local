# WP Plugin: Contact Form 7 Local JSON

Similar to ACF's [local json](https://www.advancedcustomfields.com/resources/local-json/).

## Features

Save Contact Form 7 form data to JSON and HTML

Data is stored as:

- `{theme}/wpcf7-json/{form_id}.html` - Form body
- `{theme}/wpcf7-json/{form_id}.json` - Mail settings

## Todo

- Sync, like [ACF](https://www.advancedcustomfields.com/resources/synchronized-json), with diff
- Option to set save directory - currently uses `{theme}/wpcf-json`
- Show notice on initial install
